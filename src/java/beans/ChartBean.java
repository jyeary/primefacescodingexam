/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import ejb.CustomerFacade;
import entity.Customer;
import entity.PurchaseOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author John Yeary <john.yeary@infor.com>
 */
@ManagedBean
@RequestScoped
public class ChartBean {

    private PieChartModel model;
    @EJB
    private CustomerFacade cf;

    /**
     * Creates a new instance of ChartBean
     */
    public ChartBean() {
    }

    @PostConstruct
    private void initialize() {
        Map<String, Number> salesByCustomer = new HashMap<String, Number>();

        List<Customer> customers = cf.findAll();

        for (Customer c : customers) {
            double sales = 0.0;
            for (PurchaseOrder p : c.getPurchaseOrderCollection()) {
                sales += p.getProductId().getPurchaseCost().doubleValue() * p.getQuantity();
            }
            salesByCustomer.put(c.getName(), sales);
        }

        model = new PieChartModel(salesByCustomer);
    }

    public PieChartModel getModel() {
        return model;
    }
}
